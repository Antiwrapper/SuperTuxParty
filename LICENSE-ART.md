## characters/tux
Tux model and texture

Copyright © 2010 durmieu 

Retrived from [Open Game Art](https://opengameart.org/content/tux)

License: [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)

## fonts/Boogaloo-Regular.ttf
Copyright (c) 2011, John Vargas Beltr�n� (www.johnvargasbeltran.com|john.vargasbeltran@gmail.com),
with Reserved Font Name Boogaloo.

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL

See fonts/OFL for full license.

## node/
Tiles20_col.jpg
Tiles20_nrm.jpg
Tiles20_rgh.jpg

Copyright © 2018 StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## player/
paper02_col.jpg
paper02_nrm.jpg
paper02_rgh.jpg

Copyright © 2018 StruffelProductions

Retrived from [CC0 Textures](https://cc0textures.com/home)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## minigames/FFA/knock_off
water_diffuse.png

Copyright © 2018 Bastiaan Olij

Retrived from [Github](https://github.com/BastiaanOlij/shader_tutorial)

License: [CC0 1.0 UnBastiaan Olijiversal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

## images
cake.png

Copyright © 2016 maruki

Retrived from [Open Game Art](https://opengameart.org/content/foodies)

License: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

cookie.png

Copyright © 2017 InkMammoth

Retrived from [Open Game Art](https://opengameart.org/content/pixel-art-food-pack-by-inkmammoth)

License: [GPL 2.0](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)